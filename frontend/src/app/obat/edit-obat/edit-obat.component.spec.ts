import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditObatComponent } from './edit-obat.component';

describe('EditObatComponent', () => {
  let component: EditObatComponent;
  let fixture: ComponentFixture<EditObatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditObatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditObatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
