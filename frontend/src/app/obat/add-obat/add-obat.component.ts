import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs/observable/from';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ConnectionBackend } from '@angular/http';
import { ServiceService } from '../../service.service';

@Component({
  selector: 'app-add-obat',
  templateUrl: './add-obat.component.html',
  styleUrls: ['./add-obat.component.css']
})
export class AddObatComponent implements OnInit {

  obatArr: any;
  constructor(private service : ServiceService,private router : Router) { }

  ngOnInit() {
  }

  save() {
    this.obatArr = {
      "medicineCode": (document.getElementById("medicineCode") as HTMLInputElement).value,
      "name": (document.getElementById("name") as HTMLInputElement).value,
      "type": (document.getElementById("type") as HTMLInputElement).value,
      "category": (document.getElementById("category") as HTMLInputElement).value,
      "price": (document.getElementById("price") as HTMLInputElement).value,
      "expiredDate": (document.getElementById("expiredDate") as HTMLInputElement).value,
    }
    this.service.postObat(this.obatArr).subscribe((res) => {
      this.router.navigate(["obats"])
    });
  }

}
