import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';
import { from } from 'rxjs/observable/from';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ConnectionBackend } from '@angular/http';


@Component({
  selector: 'app-obat',
  templateUrl: './obat.component.html',
  styleUrls: ['./obat.component.css']
})
export class ObatComponent implements OnInit {

  obats: any;

  constructor(private service: ServiceService, private router: Router) { }

  ngOnInit() {
    this.loadData()
  }

  loadData() {
    this.service.getObat().subscribe(data => {
      return this.obats = (data);
    });
  }
  add() {
    this.router.navigate(["obats/add"])
  }

  edit(obat): void {
    window.localStorage.removeItem("obj");
    window.localStorage.setItem("obj", obat._id + "");
    this.router.navigate(["obats/edit"]);
  }
  delete(pasien): void {
    this.service.deleteObat(pasien).subscribe((res) => {
      this.loadData();
    });
  }
}
