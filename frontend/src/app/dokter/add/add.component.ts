import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs/observable/from';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ConnectionBackend } from '@angular/http';
import { ServiceService } from '../../service.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  dokterArr: any;
  constructor(private service : ServiceService,private router : Router) { }

  ngOnInit() {
  }

  save() {
    this.dokterArr = {
      "nip": (document.getElementById("nip") as HTMLInputElement).value,
      "name": (document.getElementById("name") as HTMLInputElement).value,
      "gender": (document.getElementById("gender") as HTMLInputElement).value,
      "spesialis": (document.getElementById("spesialis") as HTMLInputElement).value,
      "address": (document.getElementById("address") as HTMLInputElement).value,
      "birthDate": (document.getElementById("birthDate") as HTMLInputElement).value,
      "phoneNumber": (document.getElementById("phoneNumber") as HTMLInputElement).value
    }
    this.service.postDokter(this.dokterArr).subscribe((res) => {
      this.router.navigate(["dokters"])
    });
  }


}
