import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../service.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  editForm: FormGroup;
  constructor(private service: ServiceService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    let _id = window.localStorage.getItem("obj");
    if (!_id) {
      this.router.navigate(["dokters"]);
      return;
    }
    this.editForm = this.formBuilder.group({
      _id: [],
      nip: [],
      name: [],
      gender: [],
      spesialis: [],
      address: [],
      phoneNumber: [],
      birthDate: [],
      __v: []

    });
    this.service.getDokterById(_id).subscribe(data => {
      this.editForm.setValue(data);
    });
  }

  onSubmit() {
    this.service.putDokter(this.editForm.value).subscribe((data) => {
      try {
        alert("Update Success");
        this.router.navigate(["dokters"])
      }
      catch (err) {
        console.log(err.message);
      }
    })
  }

}
