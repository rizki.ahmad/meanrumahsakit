import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';
import { from } from 'rxjs/observable/from';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ConnectionBackend } from '@angular/http';

@Component({
  selector: 'app-dokter',
  templateUrl: './dokter.component.html',
  styleUrls: ['./dokter.component.css']
})
export class DokterComponent implements OnInit {

  dokters: any;

  constructor(private service: ServiceService, private router: Router) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.service.getDokters().subscribe(data => {
      return this.dokters = (data);
    });
  }

  add(){
    this.router.navigate(["dokters/add"])
  }
 
  edit(dokter): void {
    window.localStorage.removeItem("obj");
    window.localStorage.setItem("obj", dokter._id + "");
    this.router.navigate(["dokters/edit"]);
  }
  delete(dokter): void {
    this.service.deleteDokter(dokter).subscribe((res) => {
      this.loadData();
    });
  }

}
