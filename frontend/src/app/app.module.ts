import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { ServiceService } from './service.service';
import { DokterComponent } from './dokter/dokter.component';
import { PasienComponent } from './pasien/pasien.component';
import { ObatComponent } from './obat/obat.component';
import { EditComponent } from './dokter/edit/edit.component';
import { AddComponent } from './dokter/add/add.component';
import { from } from 'rxjs/observable/from';
import { EditPasienComponent } from './pasien/edit-pasien/edit-pasien.component';
import { AddPasienComponent } from './pasien/add-pasien/add-pasien.component';
import { AddObatComponent } from './obat/add-obat/add-obat.component';
import { EditObatComponent } from './obat/edit-obat/edit-obat.component';
import { HeaderComponent } from './component/header/header.component';
import { FooterComponent } from './component/footer/footer.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';

const router: Routes = [
  { path: 'dokters', component: DokterComponent },
  { path: 'dokters/add', component: AddComponent },
  { path: 'dokters/edit', component: EditComponent },
  { path: 'pasiens', component: PasienComponent },
  { path: 'pasiens/add', component:AddPasienComponent },
  { path: 'pasiens/edit', component: EditPasienComponent },
  { path: 'obats', component: ObatComponent },
  { path: 'obats/add', component:AddObatComponent },
  { path: 'obats/edit', component: EditObatComponent },
]

@NgModule({
  declarations: [
    AppComponent,
    DokterComponent,
    PasienComponent,
    ObatComponent,
    EditComponent,
    AddComponent,
    AddPasienComponent,
    EditPasienComponent,
    AddObatComponent,
    EditObatComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(router),
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [RouterModule],
  providers: [ServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
