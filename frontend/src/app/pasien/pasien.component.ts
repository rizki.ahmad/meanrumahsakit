import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';
import { from } from 'rxjs/observable/from';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ConnectionBackend } from '@angular/http';

@Component({
  selector: 'app-pasien',
  templateUrl: './pasien.component.html',
  styleUrls: ['./pasien.component.css']
})
export class PasienComponent implements OnInit {

  pasiens: any;

  constructor(private service: ServiceService, private router: Router) { }

  ngOnInit() {
    this.loadData();
  }
  loadData() {
    this.service.getPasiens().subscribe(data => {
      return this.pasiens = (data);
    });
  }
  add() {
    this.router.navigate(["pasiens/add"])
  }

  edit(pasien): void {
    window.localStorage.removeItem("obj");
    window.localStorage.setItem("obj", pasien._id + "");
    this.router.navigate(["pasiens/edit"]);
  }
  delete(pasien): void {
    this.service.deletePasien(pasien).subscribe((res) => {
      this.loadData();
    });
  }
}
