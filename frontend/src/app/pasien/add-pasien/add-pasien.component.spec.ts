import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPasienComponent } from './add-pasien.component';

describe('AddPasienComponent', () => {
  let component: AddPasienComponent;
  let fixture: ComponentFixture<AddPasienComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPasienComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPasienComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
