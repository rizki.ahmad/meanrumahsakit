import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs/observable/from';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ConnectionBackend } from '@angular/http';
import { ServiceService } from '../../service.service';

@Component({
  selector: 'app-add-pasien',
  templateUrl: './add-pasien.component.html',
  styleUrls: ['./add-pasien.component.css']
})
export class AddPasienComponent implements OnInit {

  pasienArr: any;
  constructor(private service : ServiceService,private router : Router) { }

  ngOnInit() {
  }

  save() {
    this.pasienArr = {
      "nik": (document.getElementById("nik") as HTMLInputElement).value,
      "name": (document.getElementById("name") as HTMLInputElement).value,
      "gender": (document.getElementById("gender") as HTMLInputElement).value,
      "address": (document.getElementById("address") as HTMLInputElement).value,
      "birthDate": (document.getElementById("birthDate") as HTMLInputElement).value,
      "phoneNumber": (document.getElementById("phoneNumber") as HTMLInputElement).value
    }
    this.service.postPasiens(this.pasienArr).subscribe((res) => {
      this.router.navigate(["pasiens"])
    });
  }

}
