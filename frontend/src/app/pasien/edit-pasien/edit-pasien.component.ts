import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../service.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-edit-pasien',
  templateUrl: './edit-pasien.component.html',
  styleUrls: ['./edit-pasien.component.css']
})
export class EditPasienComponent implements OnInit {

  editForm: FormGroup;
  constructor(private service: ServiceService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    let _id = window.localStorage.getItem("obj");
    if (!_id) {
      this.router.navigate(["pasiens"]);
      return;
    }
    this.editForm = this.formBuilder.group({
      _id: [],
      nik: [],
      name: [],
      gender: [],
      address: [],
      phoneNumber: [],
      birthDate: [],
      __v: []

    });
    this.service.getPasiensById(_id).subscribe(data => {
      this.editForm.setValue(data);
    });
  }
  onSubmit() {
    this.service.putPasien(this.editForm.value).subscribe((data) => {
      try {
        alert("Update Success");
        this.router.navigate(["pasiens"])
      }
      catch (err) {
        console.log(err.message);
      }
    })
  }

}
