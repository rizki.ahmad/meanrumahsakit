export class Pasien {
    _id: String;
    nik: Number;
    name: String;
    gender: String;
    address: String;
    phoneNumber: String;
    birthDate: Date;
}