export class Obat {
    _id: String;
    medicineCode: String;
    name: String;
    type: String;
    category: String;
    price: Number;
    expiredDate: Date;
}