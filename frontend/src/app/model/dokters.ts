export class Dokter {
    _id: String;
    nip: Number;
    name: String;
    gender: String;
    spesialis: String;
    address: String;
    phoneNumber: String;
    birthDate: Date;
}