import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Dokter } from './model/dokters';
import { Pasien } from './model/pasiens';
import { Obat } from './model/obat';

@Injectable()
export class ServiceService {

  BASE_URL: string = 'http://localhost:8888/'

  constructor(private http: HttpClient) { }

  getDokters(): Observable<any> {
    return this.http.get<any>(this.BASE_URL + "dokters");
  }

  getDokterById(_id: String): Observable<any> {
    return this.http.get<any>(this.BASE_URL + "dokters/" + _id);
  }

  postDokter(dokter: Dokter) {
    return this.http.post(this.BASE_URL + "dokters/", dokter);
  }

  putDokter(dokter: Dokter) {
    return this.http.put(this.BASE_URL + "dokters/" + dokter._id, dokter);
  }

  deleteDokter(dokter: Dokter) {
    return this.http.delete(this.BASE_URL + "dokters/" + dokter._id);
  }

  //APi for Pasiens

  getPasiens(): Observable<any> {
    return this.http.get<any>(this.BASE_URL + "pasiens/");
  }

  getPasiensById(_id: String): Observable<any> {
    return this.http.get<any>(this.BASE_URL + "pasiens/" + _id);
  }

  postPasiens(pasien: Pasien) {
    return this.http.post(this.BASE_URL + "pasiens/", pasien);
  }

  putPasien(pasien: Pasien) {
    return this.http.put(this.BASE_URL + "pasiens/" + pasien._id, pasien);
  }

  deletePasien(pasien: Pasien) {
    return this.http.delete(this.BASE_URL + "pasiens/" + pasien._id);
  }


  //APi for Obats

  getObat(): Observable<any> {
    return this.http.get<any>(this.BASE_URL + "obats/");
  }

  getObatById(_id: String): Observable<any> {
    return this.http.get<any>(this.BASE_URL + "obats/" + _id);
  }

  postObat(obat: Obat) {
    return this.http.post(this.BASE_URL + "obats/", obat);
  }

  putObat(obat: Obat) {
    return this.http.put(this.BASE_URL + "obats/" + obat._id, obat);
  }

  deleteObat(obat: Obat) {
    return this.http.delete(this.BASE_URL + "obats/" + obat._id);
  }


}
