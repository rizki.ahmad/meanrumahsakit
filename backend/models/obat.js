const mongoose = require('mongoose');
var Obats = mongoose.model('obats', {
    medicineCode: { type: String },
    name: { type: String },
    type: { type: String },
    category: { type: String },
    price: { type: Number },
    expiredDate: { type: Date }
});
module.exports = { Obats };