const mongoose = require('mongoose');
var Dokters = mongoose.model('dokters', {
    nip: { type: Number },
    name: { type: String },
    gender: { type: String },
    spesialis: { type: String },
    address: { type: String },
    phoneNumber: { type: String },
    birthDate: { type: Date }
});
module.exports = { Dokters };