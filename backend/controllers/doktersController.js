const express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;

var { Dokters } = require('../models/dokters');

// => localhost:8888/dokters/

router.get('/', (req, res) => {
    Dokters.find((err, docs) => {
        if (!err) { res.send(docs); }
        else { console.log('Error in Retriving Dokters :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.get('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    Dokters.findById(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Retriving Dokters :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.post('/', (req, res) => {
    var dokters = new Dokters({
        nip: req.body.nip,
        name: req.body.name,
        gender: req.body.gender,
        spesialis: req.body.spesialis,
        address: req.body.address,
        phoneNumber: req.body.phoneNumber,
        birthDate: req.body.birthDate,
    });
    dokters.save((err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Dokters Save :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.put('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    var dokters = {
        nip: req.body.nip,
        name: req.body.name,
        gender: req.body.gender,
        spesialis: req.body.spesialis,
        address: req.body.address,
        phoneNumber: req.body.phoneNumber,
        birthDate: req.body.birthDate,
    };
    Dokters.findByIdAndUpdate(req.params.id, { $set: dokters }, { new: true }, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Dokters Update :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.delete('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    Dokters.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Dokters Delete :' + JSON.stringify(err, undefined, 2)); }
    });
});

module.exports = router;