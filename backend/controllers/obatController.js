const express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;

var { Obats } = require('../models/obat');

// => localhost:8888/obats/

router.get('/', (req, res) => {
    Obats.find((err, docs) => {
        if (!err) { res.send(docs); }
        else { console.log('Error in Retriving Obats :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.get('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    Obats.findById(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Retriving Obats :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.post('/', (req, res) => {
    var obats = new Obats({
        medicineCode: req.body.medicineCode,
        name: req.body.name,
        type: req.body.type,
        category: req.body.category,
        price: req.body.price,
        expiredDate: req.body.expiredDate,
    });
    obats.save((err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Obats Save :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.put('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    var obats = {
        medicineCode: req.body.medicineCode,
        name: req.body.name,
        type: req.body.type,
        category: req.body.category,
        price: req.body.price,
        expiredDate: req.body.expiredDate,
    };
    Obats.findByIdAndUpdate(req.params.id, { $set: obats }, { new: true }, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Obats Update :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.delete('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    Obats.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Obats Delete :' + JSON.stringify(err, undefined, 2)); }
    });
});

module.exports = router;