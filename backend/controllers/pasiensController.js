const express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;

var { Pasiens } = require('../models/pasiens');

// => localhost:8888/pasiens/

router.get('/', (req, res) => {
    Pasiens.find((err, docs) => {
        if (!err) { res.send(docs); }
        else { console.log('Error in Retriving Pasiens :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.get('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    Pasiens.findById(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Retriving Pasiens :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.post('/', (req, res) => {
    var pasiens = new Pasiens({
        nik: req.body.nik,
        name: req.body.name,
        gender: req.body.gender,
        address: req.body.address,
        phoneNumber: req.body.phoneNumber,
        birthDate: req.body.birthDate,
    });
    pasiens.save((err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Pasiens Save :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.put('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    var pasiens = {
        nik: req.body.nik,
        name: req.body.name,
        gender: req.body.gender,
        address: req.body.address,
        phoneNumber: req.body.phoneNumber,
        birthDate: req.body.birthDate,
    };
    Pasiens.findByIdAndUpdate(req.params.id, { $set: pasiens }, { new: true }, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Pasiens Update :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.delete('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    Pasiens.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Pasiens Delete :' + JSON.stringify(err, undefined, 2)); }
    });
});

module.exports = router;